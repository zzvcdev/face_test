import 'dart:core';
import 'dart:ffi';
import 'dart:typed_data';
import 'dart:ui';
import 'package:faceflutter/FaceDetectionMaster.dart';
import 'package:faceflutter/faceflutter.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:camera/camera.dart';
import 'dart:math' as math;

class FaceDetectPage extends StatefulWidget {
  final bool alive;

  const FaceDetectPage({Key key, this.alive:false}) : super(key: key);

  @override
  _FaceDetectPageState createState() => _FaceDetectPageState();
}

class _FaceDetectPageState extends State<FaceDetectPage> {
  CameraController controller;
  Uint8List cImageData;
  String _information = '';
  bool isDetecting = false;
  Timer timer;
  Timer faceViewTimer;


  Future initPlatformState() async {
    String information;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      information = await Faceflutter.activationEngine;
      if(information == "激活失败") {
        setState(() {
          _information = information;
        });
        return ;
      }
      information = await Faceflutter.initEngine(this.widget.alive);
      if(information == "初始化引擎失败") {
        setState(() {
          _information = information;
        });
        return;
      }
      List<dynamic> errFaceList = await Faceflutter.initLocalFaceList;
      for(int i = 0; i < errFaceList.length; i++){
        print("无法识别人脸   " + errFaceList[i].toString());
      }
    } on PlatformException {
      setState(() {
        _information = information;
      });
      return;
    }

    if (!mounted) {
      setState(() {
        _information = "无法启动引擎";
      });
      return;
    }
  }

  int _elapsedSeconds = 0;

  void startDetect() {
    try{
      controller = CameraController(cameras[0], ResolutionPreset.high);
      print("cameras = " + cameras[0].name);
      controller.initialize().then((_) {
        if (!mounted) {
          return;
        }

        print("初始化完成，开始进行人脸识别");
        faceViewTimer = Timer.periodic(Duration(seconds: 1), (timer) {
          setState(() {
            _elapsedSeconds++;
          });
          if (_elapsedSeconds == 60) {
            print("时间到60秒后，还未识别到人脸，自动关闭人脸识别界面");
            Navigator.of(context).pop();
          }
        });

        controller.startImageStream((cameraImage) async {
          if(isDetecting == false && controller != null) {
            isDetecting = true;

            print("Camera Image: ${cameraImage.width} * ${cameraImage.height}, planes: ${cameraImage.planes.length}");

            String data = await Faceflutter.detectionFace(concatenatePlanes(cameraImage.planes));


            String name = "null";
            String nameId = "";
            try{
              if(data != "null") {
                name = data.split("-")[0];
                nameId = data.split("-")[1].split('.')[0];
              }

              if (mounted) {
                setState(() { });
              }
              timer = Timer(new Duration(milliseconds: 50), (){
                isDetecting = false;
              });

              if(name != null && nameId != "") {
                controller.stopImageStream();
                Navigator.of(context).pop(data);
              }
            }catch(e) {
              print("---  出错了");
              timer = Timer(new Duration(milliseconds: 50), (){
                isDetecting = false;
              });
            }
          }
        });
      });
    } catch(e) {
      print("摄像头状态错误,请重新启动.");
    }
  }

  @override
  void dispose() {
    destroy();
    super.dispose();
  }

  void destroy() async {
    print("关闭引擎成功");
    isDetecting = true;
    faceViewTimer?.cancel();
    timer?.cancel();
    controller?.dispose();
    controller = null;
    await Faceflutter.initDetection; // Clear the face information after closing.
    await Faceflutter.unInitEngine;
  }

  @override
  void initState() {
    super.initState();

    initPlatformState();
    startDetect();
  }

  double get width => height/9.0 * 16.0;
  double get height => 450.0;

  Color get bgColor => Colors.black;

  Widget get faceDetectArea {
    return Stack(
      children: [
        Transform(
          alignment: Alignment.center,
          transform: Matrix4.rotationY(math.pi),
          child: Container(
            width: width,
            height: height,
            child: AspectRatio(
              aspectRatio: controller.value.aspectRatio,
              child: CameraPreview(controller),
            ),
          ),
        ),
        Positioned(
          left: 0,
          top: -1,
          child: Container(
            color: bgColor,
            width: width * 4.5 / 16.0,
            height: height+1,
          ),
        ),
        Positioned(
          right: 0,
          top: -1,
          child: Container(
            color: bgColor,
            width: width * 4.5 / 16.0,
            height: height+1,
          ),
        ),
      ],
    );
  }

  Widget get content {
    print("isInitialized = " + controller.value.isInitialized.toString());

    if (!controller.value.isInitialized) {
      return Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CircularProgressIndicator(),
            SizedBox(height: 10),
            Text("人脸识别正在初始化...", style: TextStyle(fontSize: 20)),
          ],
        ),
      );
    }

    return Stack(
      children: <Widget>[
        Center(
          child: faceDetectArea,
        ),
        Positioned(
          left: 0,
          bottom: 0,
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: (MediaQuery.of(context).size.height - height) / 2.0,
            color: bgColor,
            child: Center(
              child: Text("${60 - _elapsedSeconds} seconds", style: TextStyle(color: Colors.white60),),
            ),
          ),
        ),
//        Text(_information, style: TextStyle(fontSize: 25, color: Colors.lightGreen),),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: content,
    );
  }

  bool isSameUint8List(Uint8List l1, Uint8List l2) {
    if (l1.length != l2.length) return false;
    for(int i = 0; i < l1.length; i++) {
      if (l1[i] != l2[i]) return false;
    }

    return true;
  }

  Uint8List getNv21Data(List<Plane> planes){
    print("Begin type1 -- ${DateTime.now()}");
    Uint8List data1 = Uint8List.fromList([...planes[0].bytes, ...planes[1].bytes, ...planes[2].bytes]);
    print("Begin type2 -- ${DateTime.now()}");
    Uint8List data2 = concatenatePlanes(planes);
    print("end type2 -- ${DateTime.now()}, data1: ${data1.length}, data2: ${data2.length}");

    bool isSame = isSameUint8List(data1, data2);
    print("IsSAME: $isSame");

    return data1;
  }

  Uint8List concatenatePlanes(List<Plane> planes) {
    final WriteBuffer allBytes = WriteBuffer();
    int length = 0;
    planes.forEach((Plane plane) {
      allBytes.putUint8List(plane.bytes);
      length += plane.bytes.length;
    });

    return allBytes.done().buffer.asUint8List(0, length);
  }
}
